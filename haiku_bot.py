'''
Haiku Bot
by Mic Ruopp
(c) 2018


Step 1: Get comments
Step 2: Process and analyze comments
Step 3: Sleep
Step 4: Repeat

Resources:
 - http://www.whycouch.com/2014/05/how-to-create-simple-reddit-bot-in-10.html

'''

import praw
import time
import nltk
import os
import re

#nltk.download('cmudict')

#import pyphen

'''
Pyphen Notes:

- spaces -> spa-ces
  but
  spaces? -> spaces?

- what about spaces -> what about s-paces?

- it doesn't seem it works with spaces, so each 

# Pyhphen fails on the word "haiku". It was not intended for syllable divisions, but hyphenation points
# I'm going to use NLTK instead in hope of greater accuracy.
#dictionary = pyphen.Pyphen(lang='en')

'''


botname = 'kumons_haiku_bot'
subname = 'pythonforengineers'

dictionary = nltk.corpus.cmudict.dict()

succ1 = "This is a Haiku. It should strip the periods. Almost accurate."
succ2 = ""
fail1 = ""
fail2 = ""

#r = reddit = praw.Reddit(botname)
#s = r.subreddit(subname)

def gatherComments():
	for submission in s.new(limit=2):
		print(submission)

def isThisAHaiku(phrase):
	divisions = dictionary.inserted(phrase)
	print(dictionary.inserted('haiku'))
	#it doesn't count "haiku" correctly!!! I can't use this!!! back to nltk...

def countSyllables(word):
	try:
		return [len(list(y for y in x if y[-1].isdigit())) for x in dictionary[word.lower()]]
	except KeyError as err:
		print(f'Error counting syllables for "{word}"')
		print(err)

def printBotInfo():
	print(f'botname: {botname}')
	print(f'subname: {subname}')


#isThisAHaiku(succ1)

syllables = countSyllables('onomatopoeia')

print(syllables)

'''
print(dic.inserted('spaces?'))
hyphenation = dic.inserted(comment.body)
		print(hyphenation)
words = comment.body.split()
		syllables = []
		for word in words:
			syllables.append(dic.inserted(word))
		print(syllables)

'''

'''
count = 1
for submission in s.new(limit=2):
	print(f'[SUBMISSION {count}]')
	print(submission.title)
	print(submission.selftext)
	print()

	comments = submission.comments.list()
	for comment in comments:

		c = comment.body
		
		if(c != '[deleted]'):
			print(c)
			print('  --  --  --  ')

		
		It's a little closer. So I need to: 
		  split on punctuation, 
		  sub numbers for their words,
		  once each word is accounted for, run it through pyphen, and then split results on the hyphens,
		  count these results to get the number of syllables

		  thought also needs to go into how these are counted.
		  Since the bot is trying to make haiku's, it can't count 5 syllables where the fifth is the first half of a word
		  Not only does it need to be the correct count of syllables, it needs to also be full words
		

	print()
	print()
	count += 1

'''

